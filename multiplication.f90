program multiplication
    !$ use omp_lib 
    implicit none
    save

    integer :: seed, i, j, n
    real :: x, summ
    real(8) :: t0, t1
    character(100) :: option
    real, dimension(10000,10000) :: matrix
    real, dimension(10000,1) :: xvector
    real, dimension(10000,1) :: yvector

    n = size(xvector)

    do i=1,n
        do j=1,n
            call RANDOM_NUMBER(x)
            matrix(i,j) = x
        end do
        call RANDOM_NUMBER(x)
        xvector(i,1) = x
    end do

    t0 = 0.0d0
    !$ t0 = omp_get_wtime();
    !$omp parallel default(none) private(i,j,summ) shared(n,xvector,yvector,matrix) 
    !$omp do schedule(static) reduction(+:yvector)
    !compute zith dynamic in schedule()
    do i=1,n
        summ = 0
        do j=1,n
            summ = summ + matrix(i,j)*xvector(j,1)
        end do
        yvector(i,1) = yvector(i,1) + summ
    end do
    !$omp end do
    !$omp end parallel 
    t1 = 0.0d0
    !$ t1 = omp_get_wtime();
    write(*,'(a,f0.3,a)') " Time: ", t1 - t0, "s"

    !write(*,*) size(matrix), size(xvector), size(yvector)
    !write(*,*) xvector(1:6,1)
end program