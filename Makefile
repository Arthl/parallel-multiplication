FCFLAGS = -O2 -fopenmp
FC = gfortran

ALL = multiplication

all: $(ALL)

multiplication: multiplication.o
	$(FC) $(FCFLAGS) $^ -o $@
	export OMP_NUM_THREADS=1; ./$@
	export OMP_NUM_THREADS=2; ./$@
	export OMP_NUM_THREADS=4; ./$@
	export OMP_NUM_THREADS=8; ./$@
	export OMP_NUM_THREADS=12; ./$@	
	export OMP_NUM_THREADS=16; ./$@
	export OMP_NUM_THREADS=20; ./$@	
	export OMP_NUM_THREADS=26; ./$@

%.o:%.f90
	$(FC) $(FCFLAGS) -c $^

.PHONY: clean
clean:
	rm -f *.o *~
	rm -f $(ALL)
